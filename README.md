# BI-ZUM homeworks
## Homework 1
- Go to the `hw1` folder
```sh
cd hw1
```
- Install requirements
```sh
pip install -r requirements.txt
```
- Run program
```sh
python algorithms.py
```
- Select the algorithm
- Type in labyrinth name (without .txt)