import heapq as priority_queue
from termcolor import colored
import numpy as np
import os
import time


def print_labyrinth(l):
    l = ["".join(row) for row in l]
    l = "\n".join(l)
    for c in l:
        if c == '\n':
            print('\n', end='', sep='')
        elif c == 'X':
            print(colored('X', 'white'), end='', sep='')
        elif c == '#':
            print(colored('#', 'yellow'), end='', sep='')
        elif c == 'o':
            print(colored('o', 'red'), end='', sep='')
        elif c == ' ':
            print(' ', end='', sep='')
        elif c == 'S':
            print(colored('S', 'magenta'), end='', sep='')
        elif c == 'E':
            print(colored('E', 'magenta'), end='', sep='')
        elif c == 'C':
            print(colored('C', 'red'), end='', sep='')
    print()


def neighbours(position):
    x, y = position
    return [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]


def extended_neighbours(position):
    x, y = position
    return {(x + 1, y): 1, (x - 1, y): 1, (x, y + 1): 1, (x, y - 1): 1,
            (x + 1, y + 1): np.sqrt(2), (x + 1, y - 1): np.sqrt(2), (x - 1, y + 1): np.sqrt(2),
            (x - 1, y - 1): np.sqrt(2)}


def bfs(l, start, finish):
    q = [start]
    visited = set(q)
    came_from = {}

    while len(q) > 0:
        os.system('cls' if os.name == 'nt' else 'clear')
        p = q.pop(0)
        x, y = p
        l[y][x] = 'C'

        if p == finish:
            return came_from

        for neighbour in neighbours(p):
            if neighbour not in visited and l[neighbour[1]][neighbour[0]] != 'X':
                q.append(neighbour)
                visited.add(neighbour)
                came_from[neighbour] = p

        print_labyrinth(l)
        l[y][x] = '#'
        time.sleep(0.1)

    return None


def dfs(l, start, finish):
    q = [start]
    visited = set(q)
    came_from = {}

    while len(q) > 0:
        os.system('cls' if os.name == 'nt' else 'clear')
        p = q.pop()
        x, y = p
        l[y][x] = 'C'

        if p == finish:
            return came_from

        for neighbour in neighbours(p):
            if neighbour not in visited and l[neighbour[1]][neighbour[0]] != 'X':
                q.append(neighbour)
                visited.add(neighbour)
                came_from[neighbour] = p

        print_labyrinth(l)
        l[y][x] = '#'
        time.sleep(0.1)

    return None


def random_search(l, start, finish):
    q = [start]
    visited = set(q)
    came_from = {}

    while len(q) > 0:
        np.random.shuffle(q)
        os.system('cls' if os.name == 'nt' else 'clear')
        p = q.pop()
        x, y = p
        l[y][x] = 'C'

        if p == finish:
            return came_from

        for neighbour in neighbours(p):
            if l[neighbour[1]][neighbour[0]] != 'X' and neighbour not in visited:
                q.append(neighbour)
                visited.add(neighbour)
                came_from[neighbour] = p

        print_labyrinth(l)
        l[y][x] = '#'
        time.sleep(0.1)

    return None


def dijkstra(l, start, finish):
    q = []
    priority_queue.heappush(q, (0, start))

    visited = set(start)
    came_from = {}
    costs = {start: 0}

    while len(q) > 0:
        os.system('cls' if os.name == 'nt' else 'clear')
        p = q[0]
        priority_queue.heappop(q)
        x, y = p[1]
        steps = p[0]
        visited.add(p[1])
        l[y][x] = 'C'

        if p[1] == finish:
            return came_from

        for neighbour in extended_neighbours(p[1]).keys():
            new_cost = steps + extended_neighbours(p[1])[neighbour]
            if l[neighbour[1]][neighbour[0]] != 'X' and (neighbour not in costs or new_cost < costs[neighbour]):
                costs[neighbour] = new_cost
                priority_queue.heappush(q, (new_cost, neighbour))
                came_from[neighbour] = p[1]

        print_labyrinth(l)
        l[y][x] = '#'
        time.sleep(0.05)

    return None


def greedy_search(l, start, finish):
    q = []
    priority_queue.heappush(q, (0, start))
    came_from = {}
    costs = {start: 0}

    while len(q) > 0:
        os.system('cls' if os.name == 'nt' else 'clear')
        p = q[0]
        priority_queue.heappop(q)
        x, y = p[1]
        l[y][x] = 'C'

        if p[1] == finish:
            return came_from

        for neighbour in extended_neighbours(p[1]):
            new_cost = l1_norm(neighbour, finish)
            if l[neighbour[1]][neighbour[0]] != 'X' and (neighbour not in costs or new_cost < costs[neighbour]):
                costs[neighbour] = new_cost
                priority_queue.heappush(q, (new_cost, neighbour))
                came_from[neighbour] = p[1]

        print_labyrinth(l)
        l[y][x] = '#'
        time.sleep(0.05)

    return None


def l1_norm(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return abs(x2 - x1) + abs(y2 - y1)


def a_star(l, start, finish):
    q = []
    priority_queue.heappush(q, (0, start))
    came_from = {}
    costs = {start: 0}

    while len(q) > 0:
        os.system('cls' if os.name == 'nt' else 'clear')
        p = q[0]
        priority_queue.heappop(q)
        x, y = p[1]
        steps = costs[p[1]]
        l[y][x] = 'C'

        if p[1] == finish:
            return came_from

        for neighbour in extended_neighbours(p[1]).keys():
            print(extended_neighbours(p[1])[neighbour])
            new_cost = steps + extended_neighbours(p[1])[neighbour]
            if l[neighbour[1]][neighbour[0]] != 'X' and (neighbour not in costs or new_cost < costs[neighbour]):
                costs[neighbour] = new_cost
                priority_queue.heappush(q, (new_cost + l1_norm(neighbour, finish), neighbour))
                came_from[neighbour] = p[1]

        print_labyrinth(l)
        l[y][x] = '#'
        time.sleep(0.05)

    return None


def run(algorithm, l):
    labyrinths_folder = 'labyrinths/'
    labyrinth = open(labyrinths_folder + l + '.txt').read().splitlines()
    start, end = labyrinth[-2:]
    print(start.strip('start').split(','))
    labyrinth = labyrinth[:-2]
    start = tuple(map(int, start.strip('start').split(',')))
    end = tuple(map(int, end.strip('end').split(',')))

    if start == end:
        print('Start is a finish')
        return

    labyrinth = [list(row) for row in labyrinth]

    history = algorithm(labyrinth, start, end)

    steps = 1
    labyrinth[end[1]][end[0]] = 'E'
    pos = history[end]

    while pos != start:
        labyrinth[pos[1]][pos[0]] = 'o'
        pos = history[pos]
        steps += 1

    labyrinth[start[1]][start[0]] = 'S'

    labyrinth = ["".join(row) for row in labyrinth]
    labyrinth = "\n".join(labyrinth)

    node_expanded = 0

    for char in labyrinth:
        if char == '\n':
            print('\n', end='', sep='')
        elif char == 'X':
            print(colored('X', 'white'), end='', sep='')
        elif char == '#':
            print(colored('#', 'yellow'), end='', sep='')
            node_expanded += 1
        elif char == 'o':
            print(colored('o', 'red'), end='', sep='')
            node_expanded += 1
        elif char == ' ':
            print(' ', end='', sep='')
        elif char == 'S':
            print(colored('S', 'magenta'), end='', sep='')
        elif char == 'E':
            print(colored('E', 'magenta'), end='', sep='')
    print()
    print('-' * 40)
    print('S Start')
    print('E End')
    print('# Opened node')
    print('o Path')
    print('X Wall')
    print('space Fresh node')
    print('-' * 40)
    print('Path length:', steps)
    print('Nodes expanded:', node_expanded)
    print('Press ENTER to continue...')
    while input() != '':
        pass


while 1:
    os.system('cls' if os.name == 'nt' else 'clear')
    print('Choose the algorithm: ')
    print('- 1. BFS')
    print('- 2. DFS')
    print('- 3. Random search')
    print('- 4. Dijkstra')
    print('- 5. Greedy search')
    print('- 6. A star')
    print('- Enter \'q\' to exit')
    user_input = input('> ')
    if user_input == '1':
        print('  - Enter map name (without .txt):')
        name = input('  > ')
        run(bfs, name)
    elif user_input == '2':
        print('  - Enter map name (without .txt):')
        name = input('  > ')
        run(dfs, name)
    elif user_input == '3':
        print('  - Enter map name (without .txt):')
        name = input('  > ')
        run(random_search, name)
    elif user_input == '4':
        print('  - Enter map name (without .txt):')
        name = input('  > ')
        run(dijkstra, name)
    elif user_input == '5':
        print('  - Enter map name (without .txt):')
        name = input('  > ')
        run(greedy_search, name)
    elif user_input == '6':
        print('  - Enter map name (without .txt):')
        name = input('  > ')
        run(a_star, name)
    elif user_input == 'q':
        break
    else:
        print('> Unknown command, try again.')
    time.sleep(1)
    os.system('cls' if os.name == 'nt' else 'clear')
