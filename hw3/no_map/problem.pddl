(define (problem zum-hw3)
	(:domain hw3)

	(:objects
		boat frigate caravel tree gold-nugget gold-coin gold-bar
		alcohol drunk very-drunk addicted forest river flowers
		strong toughened bear-hide map dubious-acquaintances criminal-record
		port coconuts knows-contrabandits bar well-known
		city contacts port-academy captain sea won-pirates perl wife
		beacon island cocaine ring happy
	)

	(:init
		(connected-by-ground forest river)
        (connected-by-ground river forest)
		(connected-by-ground river port)
		(connected-by-ground port river)
		(connected-by-ground port bar)
		(connected-by-ground bar port)
		(connected-by-ground port city)
		(connected-by-ground city port)
		(connected-by-ground port-academy city)
		(connected-by-ground city port-academy)
		(connected-by-sea port beacon)
		(connected-by-sea beacon port)
		(connected-by-sea port sea)
		(connected-by-sea sea port)
		(connected-by-sea beacon sea)
		(connected-by-sea sea beacon)
		(connected-by-sea sea island)
		(connected-by-sea island sea)
		(at port)

	)

	(:goal (and
		(is happy)
	))
)

