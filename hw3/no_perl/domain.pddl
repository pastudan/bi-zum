; This is a sample domain to demonstrate template-based problem file generation

(define (domain hw3)

    (:requirements :strips :fluents :typing :negative-preconditions)

    (:predicates
        (connected-by-ground ?from ?to)
        (connected-by-sea ?from ?to)

        (at ?location)
        (has ?item)
        (is ?property)
    )

    ; Moves
    (:action move-by-ground
        :parameters (?from ?to)
        :precondition (and
            (at ?from)
            (connected-by-ground ?from ?to)
        )
        :effect (and
            (at ?to)
	        (not (at ?from))
        )
	)

    (:action move-with-boat
        :parameters (?from ?to)
        :precondition (and
            (at ?from) 
            (connected-by-sea ?from ?to)
            (has boat)
        )
        :effect (and
            (at ?to)
            (not (at ?from))
        )
	)

     (:action move-with-frigate
        :parameters (?from ?to)
        :precondition (and
            (at ?from)
            (connected-by-sea ?from ?to)
            (has frigate)
        )
        :effect (and
            (at ?to)
		    (not (at ?from))
        )
	)

    (:action move-with-caravel
        :parameters (?from ?to)
        :precondition (and
            (na ?from)
            (connected-by-sea ?from ?to)
            (ma karavela)
        )
        :effect (and
            (at ?to)
		    (not (at ?from))
        )
	)


    ; Boats
    (:action build-boat
        
        :precondition (and
            (has tree)
        )
        :effect (and
            (not (has tree))
            (has boat)
        )
    )

    (:action build-frigate
        
        :precondition (and
            (has boat)
            (has gold-nugget)
            (has tree)
        )
        :effect (and
            (not (has boat))
            (not (has gold-nugget))
            (not (has tree))
            (has frigate)
        )
    )

    (:action build-caravel
        
        :precondition (and
            (has boat)
            (has gold-coin)
            (has tree)
        )
        :effect (and
            (not (has boat))
            (not (has gold-coin))
            (not (has tree))
            (has caravel)
        )
    )



    ; Drunkenness
    (:action get-drunk
        
        :precondition (and
            (has alcohol)
            (not (is drunk))
            (not (is very-drunk))
        )
        :effect (and
            (is drunk)
            (not (has alcohol))
        )
    )

    (:action get-very-drunk
        
        :precondition (and
            (has alcohol)
            (not (is very-drunk))
        )
        :effect (and
            (is very-drunk)
            (not (has alcohol))
        )
    )

    (:action get-addicted
        
        :precondition (and
            (has alcohol)
            (is very-drunk)
        )
        :effect (and
            (is addicted)
            (not (has alcohol))
        )
    )


    ; Forest
    (:action cut-tree
        
        :precondition (and
            (at forest)
        )
        :effect (and
            (has tree)
        )
    )

    (:action get-flowers
        
        :precondition (and
            (at forest)
        )
        :effect (and
            (has flowers)
        )
    )

    (:action fight-bear
        
        :precondition (and
            (at forest)
        )
        :effect (and
            (is strong)
            (is toughened)
            (has bear-hide)
        )
    )

    (:action meet-grandfather
        
        :precondition (and )
        :effect (and
            (not (has alcohol))
            (has map)
            (has dubious-acquaintances)
        )
    )

    ; River
    (:action steal-boat
        
        :precondition (and 
            (at river)
        )
        :effect (and
            (has boat)
            (has criminal-record)
        )
    )

    (:action yield-gold
        
        :precondition (and 
            (at river)
        )
        :effect (and
            (has gold-nugget)
        )
    )

    (:action pludge-into-font
        
        :precondition (and 
            (at river)
        )
        :effect (and
            (not (is drunk))
        )
    )

    ; Port 
    (:action do-work
        
        :precondition (and 
            (at port)
        )
        :effect (and
            (has gold-nugget)
        )
    )

    (:action sell-coconuts
        
        :precondition (and 
            (at port)
            (has coconuts)
        )
        :effect (and
            (has gold-coin)
        )
    )

    (:action sell-bear-hide
        
        :precondition (and 
            (at port)
            (has bear-hide)
        )
        :effect (and
            (has gold-coin)
        )
    )

    (:action contrabandits
        
        :precondition (and 
            (at port)
            (has dubious-acquaintances)
            (has gold-bar)
        )
        :effect (and
            (is knows-contrabandits)
        )
    )

    ; Bar
    (:action buy-alcohol
        
        :precondition (and 
            (at bar)
            (has gold-nugget)
        )
        :effect (and
            (has alcohol)
        )
    )

    (:action pay-for-everyone
        
        :precondition (and 
            (at bar)
            (has gold-coin)
        )
        :effect (and
            (not (has gold-coin))
            (is well-known)
        )
    )

    (:action bar-fight
        
        :precondition (and 
            (at bar)
            (is drunk)
        )
        :effect (and
            (is toughened)
        )
    )

    ; City
    (:action accumulate-money
        
        :precondition (and 
            (at city)
            (has gold-nugget)
        )
        :effect (and
            (not (has gold-nugget))
            (has contacts)
            (has gold-coin)
        )
    )

    (:action invest-money
        
        :precondition (and 
            (at city)
            (has contacts)
            (has gold-coin)
        )
        :effect (and
            (has gold-bar)
        )
    )

    (:action rip-off
        
        :precondition (and 
            (at city)
        )
        :effect (and
            (has gold-coin)
            (has criminal-record)
        )
    )

    (:action give-bribe
        
        :precondition (and 
            (at city)
            (has criminal-record)
            (has gold-nugget)
        )
        :effect (and
            (not (has criminal-record))
            (not (has gold-nugget))
        )
    )

    (:action do-correctional-work
        
        :precondition (and 
            (at city)
            (has criminal-record)
        )
        :effect (and
            (not (has criminal-record))
            (is drunk)
        )
    )

    ; Port academy
    (:action become-captain
        
        :precondition (and 
            (at port-academy)
            (not (has criminal-record))
            (has gold-coin)
        )
        :effect (and
            (is captain)
            (is strong)
            (not (has gold-coin))
        )
    )

    ; Sea
    (:action meet-pirates
        
        :precondition (and 
            (at sea)
            (not (is toughened))
        )
        :effect (and
            (is toughened)
            (not (has gold-nugget))
            (not (has gold-coin))
            (not (has gold-bar))
            (not (has frigate))
            (not (has caravel))
        )
    )

    (:action fight-pirates
        
        :precondition (and 
            (at sea)
            (is toughened)
            (has caravel)
        )
        :effect (and
            (has gold-nugget)
            (has gold-coin)
            (has gold-bar)
            (has boat)
            (has frigate)
            (is strong)
            (has won-pirates)
        )
    )

    (:action become-pirate
        
        :precondition (and 
            (at sea)
            (has criminal-record)
        )
        :effect (and
            (is drunk)
        )
    )

    (:action get-perl
        
        :precondition (and 
            (at sea)
        )
        :effect (and
            
        )
    )

    (:action swim
        
        :precondition (and 
            (at sea)
        )
        :effect (and
            (not (is very-drunk))
            (not (is drunk))
        )
    )

    ; Beacon
    (:action marry-girl
        
        :precondition (and 
            (at beacon)
            (is strong)
        )
        :effect (and
            (has wife)
        )
    )

    ; Island
    (:action cut-tree-on-island
        
        :precondition (and 
            (at island)
        )
        :effect (and
            (has tree)
        )
    )

    (:action gather-coconuts
        
        :precondition (and 
            (at island)
        )
        :effect (and
            (has coconuts)
        )
    )

    (:action find-cocaine
        
        :precondition (and 
            (at island)
            (has map)
        )
        :effect (and
            (has cocaine)
        )
    )

    ; Final
    (:action make-ring
        
        :precondition (and 
            (has gold-bar)
            (has perl)
        )
        :effect (and
            (has ring)
        )
    )

    (:action marriage
        
        :precondition (and 
            (at island)
            (has wife)
            (has ring)
            (has flowers)
            (has contacts)
            (not (has criminal-record))
            (not (is very-drunk))
            (not (is addicted))
        )
        :effect (and
            (is happy)
        )
    )

    (:action become-admiral
        
        :precondition (and 
            (at port-academy)
            (is captain)
            (has won-pirates)
            (not (is drunk))
            (not (is very-drunk))
            (not (is addicted))
        )
        :effect (and
            (is happy)
        )
    )

    (:action become-drug-addicted
        
        :precondition (and 
            (has cocaine)
            (is addicted)
            (has frigate)
            (is knows-contrabandits)
            (has gold-bar)
        )
        :effect (and
            (is happy)
        )
    )
)